#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from pyembed.markdown import PyEmbedMarkdown
from fontawesome_markdown import FontAwesomeExtension
from embedly_cards import EmbedlyCardExtension
from markdown_newtab import NewTabExtension

MD_EXTENSIONS = [PyEmbedMarkdown(),
                 'superscript',
                 FontAwesomeExtension(),
                 'fenced_code',
                 'smarty',
                 'codehilite(css_class=highlight, linenums=False)', 
                 'extra',
                 EmbedlyCardExtension(),
                 NewTabExtension(),
                 ]

AUTHOR = u'Low And Slow Team'
SITENAME = u'Low And Slow Team'
SITEURL = 'http://localhost:8000'

THEME = 'themes/voidy-bootstrap'
#Extra stylesheets, for bootstrap overrides or additional styling
STYLESHEET_FILES = ("pygments/solarizeddark.css", "voidybootstrap_LAST.css",)
CUSTOM_SITE_HEADERS = ("jumbotron_empty.html",)


TIMEZONE = 'America/New_York'

DEFAULT_LANG = u'en'

DATE_FORMATS = {
    'en': ('en_US.utf8','%Y-%B-%d'),
}

CACHE_CONTENT = False
LOAD_CONTENT_CACHE = False 
CACHE_PATH = 'cache'

PATH = 'content'
STATIC_PATHS= ['images', 'images/reviews', 
               'css',
               'js', 
               'pdfs', 
               'favicon/favicon.ico', 
               'deploy',]

SIDEBAR = 'sidebar.html'
SIDEBAR_HIDE_CATEGORIES = True
SIDEBAR_HIDE_TAGS = False


DEFAULT_PAGINATION = 10


DISPLAY_PAGES_ON_MENU = False
MENUITEMS = [('News', '/category/news/'),
             ('About', '/pages/about/'),
             ]

PAGE_URL = 'pages/{slug}/'
PAGE_SAVE_AS = 'pages/{slug}/index.html'
#PAGE_ORDER_BY = 'page_order'

ARTICLE_URL = 'posts/{date:%Y}/{date:%b}/{date:%d}/{slug}.html'
ARTICLE_SAVE_AS = 'posts/{date:%Y}/{date:%b}/{date:%d}/{slug}.html'
ARTICLE_ORDER_BY = 'date'

CATEGORY_URL = "category/{slug}"
CATEGORY_SAVE_AS = "category/{slug}/index.html" 

TAG_URL = "tag/{slug}/"
TAG_SAVE_AS = "tag/{slug}/index.html"

AUTHOR_URL = ''
AUTHOR_SAVE_AS = ''

YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/{date:%b}/index.html'

NEWEST_FIRST_ARCHIVES = True 
REVERSE_CATEGORY_ORDER = True

#Thumbnailer settings
IMAGE_PATH = 'images'
THUMBNAIL_DIR = 'images/t'
THUMBNAIL_SIZES = {
    '100x': '100x?',
    '200x': '200x?',
}
THUMBNAIL_KEEP_NAME = True

PLUGIN_PATHS = ['plugins']
PLUGINS = [
    'categorytpl',
    'embedly_cards',
    'feed_summary',
    'google_embed',
    'gravatar',
    'liquid_tags.img',
    'liquid_tags.include_code',
    'liquid_tags.video',
    'liquid_tags.youtube',
    'minification',
    'pelican-open_graph',
    'pelican-page-order',
    'pelican_gist',
#    'photos',
    'series',
    'subcategory',
    'summary',
    'thumbnailer',
  #  'touch',
    'pin_to_top',
]

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
